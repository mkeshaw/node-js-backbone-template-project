(function() {
  (function() {
    return (function(exports) {
      exports.greetingMessage = {
        greeting: "Hello from Node JS"
      };
      return exports.greetingMessageList = [
        {
          greeting: "Good morning"
        }, {
          greeting: "Good evening"
        }
      ];
    })((typeof process === "undefined" || !process.versions ? window.greetingMessages = window.greetingMessages || {} : exports));
  }).call(this);

}).call(this);
