(function() {
  describe("Message", function() {
    describe("MessageModel", function() {
      beforeEach(function() {
        var _this = this;

        this.waitForServiceReturn = function() {
          return typeof _this.serviceReturn !== 'undefined';
        };
        return this.messageModel = new MessageModel({
          id: 1
        });
      });
      afterEach(function() {
        return $('#fixtureContainer').empty();
      });
      it("should have default properties", function() {
        return expect(this.messageModel.get("greeting")).toEqual("Hello from Backbone");
      });
      it("should have a url field that maps onto a server route", function() {
        return expect(this.messageModel.url).toEqual("/api/message");
      });
      return it("should have different content in the greeting field after fetching from the server", function() {
        var _this = this;

        this.messageModel.fetch({
          success: function(model, response, options) {
            return _this.serviceReturn = true;
          }
        });
        waitsFor(this.waitForServiceReturn, 'service did not return', 1000);
        return runs(function() {
          expect(_this.messageModel.get("greeting")).not.toEqual("Hello from Backbone");
          return expect(_this.messageModel.get("greeting")).toEqual(greetingMessages.greetingMessage.greeting);
        });
      });
    });
    describe("MessageList", function() {
      beforeEach(function() {
        this.waitForServiceReturn = function() {
          return typeof this.serviceReturn !== 'undefined';
        };
        return this.messageList = new MessageList();
      });
      afterEach(function() {
        return $('#fixtureContainer').empty();
      });
      it("should initially be empty", function() {
        return expect(this.messageList.length).toEqual(0);
      });
      it("should specify MessageModel as its model attribute", function() {
        return expect(this.messageList.model).toEqual(MessageModel);
      });
      it("should have a url field that maps onto a server route", function() {
        return expect(this.messageList.url).toEqual("/api/messages");
      });
      return it("should contain an array of MessageModel objects after calling fetch()", function() {
        var _this = this;

        this.messageList.fetch({
          success: function(model, response, options) {
            return _this.serviceReturn = true;
          }
        });
        waitsFor(this.waitForServiceReturn, 'service did not return', 1000);
        return {
          runs: function() {
            var actualFirstMessage, expectedFirstMessage;

            expect(_this.messageList.length).toBeGreaterThan(0);
            actualFirstMessage = _this.messageList.get(0);
            expectedFirstMessage = greetingMessages.greetingMessageList[0];
            return expect(actualFirstMessage.get("greeting")).toEqual(expectedFirstMessage.greeting);
          }
        };
      });
    });
    describe("MessageController", function() {
      return describe("Default initialization", function() {
        beforeEach(function() {
          this.fixture = $('#fixtureContainer');
          this.messageController = new MessageController({
            el: this.fixture,
            model: new MessageModel()
          });
          return this.messageController.render();
        });
        afterEach(function() {
          return $('#fixtureContainer').empty();
        });
        return it("should have template content that is rendered into its element", function() {
          expect(this.fixture.html()).not.toEqual("");
          return expect(this.fixture.html()).toContain("Greeting: Hello from Backbone");
        });
      });
    });
    return describe("MessageListController", function() {
      return describe("Default initialization", function() {
        beforeEach(function() {
          var messageList;

          this.fixture = $('#fixtureContainer');
          messageList = new MessageList();
          this.messageListController = new MessageListController({
            el: this.fixture,
            collection: messageList
          });
          messageList.set(greetingMessages.greetingMessageList);
          return this.messageListController.render();
        });
        afterEach(function() {
          return $('#fixtureContainer').empty();
        });
        it("should have template content that is rendered into its element", function() {
          expect(this.fixture.html()).not.toEqual("");
          return expect(this.fixture.html()).toContain("Number of greetings");
        });
        it("should render all the elements in it's collection", function() {
          var greetingList;

          greetingList = this.messageListController.$('.bv_listOfGreetings li');
          expect(greetingList.length).toEqual(2);
          return expect($(greetingList[0]).html()).toContain("Good morning");
        });
        return it("should display the number of items in the list", function() {
          return expect($('.bv_itemCount').html()).toContain("2");
        });
      });
    });
  });

}).call(this);
