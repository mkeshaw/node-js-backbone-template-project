(function() {
  describe("AppController", function() {
    describe("On initialization", function() {
      beforeEach(function() {
        this.fixture = $('#fixtureContainer');
        this.appController = new AppController({
          el: this.fixture
        });
        return this.appController.render();
      });
      afterEach(function() {
        return $('#fixture').empty();
      });
      it("should have template content that is rendered into its element", function() {
        return expect(this.fixture.html()).not.toEqual("");
      });
      return it("should display 'Hello World' in the template content", function() {
        return expect(this.fixture.html()).toContain("Hello, World");
      });
    });
    return describe("When initialized with a MessageController", function() {
      beforeEach(function() {
        this.fixture = $('#fixtureContainer');
        this.appController = new AppController({
          el: this.fixture
        });
        return this.appController.render();
      });
      it("should have template content that is rendered into its element", function() {
        var _this = this;

        waits(100);
        return {
          runs: function() {
            expect(_this.fixture.html()).not.toEqual("");
            return expect(_this.fixture.html()).toContain("Number of greetings");
          }
        };
      });
      return it("should render all the elements in it's collection", function() {
        var _this = this;

        waits(100);
        return {
          runs: function() {
            var greetingList;

            greetingList = _this.messageListController.$('.bv_listOfGreetings li');
            expect(greetingList.length).toEqual(2);
            return expect($(greetingList[0]).html()).toContain("Good morning");
          }
        };
      });
    });
  });

}).call(this);
