(function() {
  var _ref, _ref1, _ref2, _ref3,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.MessageModel = (function(_super) {
    __extends(MessageModel, _super);

    function MessageModel() {
      _ref = MessageModel.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    MessageModel.prototype.url = '/api/message';

    MessageModel.prototype.defaults = {
      greeting: "Hello from Backbone"
    };

    MessageModel.prototype.initialize = function() {};

    return MessageModel;

  })(Backbone.Model);

  window.MessageList = (function(_super) {
    __extends(MessageList, _super);

    function MessageList() {
      _ref1 = MessageList.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    MessageList.prototype.url = '/api/messages';

    MessageList.prototype.model = MessageModel;

    return MessageList;

  })(Backbone.Collection);

  window.MessageController = (function(_super) {
    __extends(MessageController, _super);

    function MessageController() {
      this.render = __bind(this.render, this);      _ref2 = MessageController.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    MessageController.prototype.template = $('#message-cotroller-template');

    MessageController.prototype.tagName = 'li';

    MessageController.prototype.render = function() {
      var template;

      this.$el.empty();
      template = _.template($(this.template).html(), this.model.toJSON());
      this.$el.html(template);
      return this;
    };

    return MessageController;

  })(Backbone.View);

  window.MessageListController = (function(_super) {
    __extends(MessageListController, _super);

    function MessageListController() {
      this.addItem = __bind(this.addItem, this);
      this.addAllItems = __bind(this.addAllItems, this);
      this.render = __bind(this.render, this);      _ref3 = MessageListController.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    MessageListController.prototype.template = $('#message-list-controller-template').html();

    MessageListController.prototype.initialize = function() {
      this.collection.bind("fetch", this.render);
      this.collection.bind("change", this.render);
      return this.collection.bind("add", this.render);
    };

    MessageListController.prototype.render = function() {
      var template;

      this.$el.empty();
      template = _.template(this.template, {
        itemCount: this.collection.length
      });
      this.$el.html(template);
      this.addAllItems();
      return this;
    };

    MessageListController.prototype.addAllItems = function() {
      var _this = this;

      return this.collection.each(function(item) {
        return _this.addItem(item);
      });
    };

    MessageListController.prototype.addItem = function(item) {
      var messageItem;

      messageItem = new MessageController({
        model: item
      });
      return this.$('.bv_listOfGreetings').append(messageItem.render().el);
    };

    return MessageListController;

  })(Backbone.View);

}).call(this);
