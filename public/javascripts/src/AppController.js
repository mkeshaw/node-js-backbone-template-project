(function() {
  var _ref,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.AppController = (function(_super) {
    __extends(AppController, _super);

    function AppController() {
      this.render = __bind(this.render, this);      _ref = AppController.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    AppController.prototype.template = _.template($('#app-template').html());

    AppController.prototype.initialize = function() {
      $(this.el).html(this.template);
      this.messageController = new MessageController({
        el: this.$(".bv_messageContainer"),
        model: new MessageModel()
      });
      this.messageList = new MessageList();
      this.messageListController = new MessageListController({
        el: this.$(".bv_messageListContainer"),
        collection: this.messageList
      });
      return this.messageList.fetch();
    };

    AppController.prototype.render = function() {
      this.messageController.render();
      return this;
    };

    return AppController;

  })(Backbone.View);

}).call(this);
