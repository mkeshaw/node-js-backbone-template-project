describe "AppController", ->
	describe "On initialization", ->
		beforeEach ->
			@fixture = $('#fixtureContainer')
			@appController = new AppController({el: @fixture})
			@appController.render()

		afterEach ->
			$('#fixture').empty()


		it "should have template content that is rendered into its element", ->
			# after the appController has been initialized and render has been called,
			# it's element should now contain its template content
			expect(@fixture.html()).not.toEqual("")

		it "should display 'Hello World' in the template content", ->
			# verify that some specific content is present in the now rendered template
			expect(@fixture.html()).toContain("Hello, World")

	describe "When initialized with a MessageController", ->
		beforeEach ->
			@fixture = $('#fixtureContainer')
			@appController = new AppController el: @fixture
			@appController.render()

		it "should have template content that is rendered into its element", ->
			waits 100
			runs: =>
				expect(@fixture.html()).not.toEqual("")
				expect(@fixture.html()).toContain("Number of greetings")

		it "should render all the elements in it's collection", ->
			waits 100
			runs: =>
				greetingList = @messageListController.$('.bv_listOfGreetings li')
				expect(greetingList.length).toEqual(2)
				expect($(greetingList[0]).html()).toContain("Good morning")
