class window.AppController extends Backbone.View
	template: _.template($('#app-template').html())

	initialize: ->
		$(@el).html(@template)
		@messageController = new MessageController
			el: @$(".bv_messageContainer")
			model: new MessageModel()
		@messageList = new MessageList()
		@messageListController = new MessageListController
			el: @$(".bv_messageListContainer")
			collection: @messageList
		@messageList.fetch()

	render: =>
		@messageController.render()

		@