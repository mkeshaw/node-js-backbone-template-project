(->
	((exports) ->
		exports.greetingMessage =
			greeting: "Hello from Node JS"
		exports.greetingMessageList = [
			greeting: "Good morning"
		,
			greeting: "Good evening"
		]
	) ((if typeof process is "undefined" or not process.versions then window.greetingMessages = window.greetingMessages or {} else exports))
).call this