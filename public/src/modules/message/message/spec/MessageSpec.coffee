describe "Message", ->
	describe "MessageModel", ->
		beforeEach ->

			@waitForServiceReturn = =>
				typeof @serviceReturn != 'undefined'
			@messageModel = new MessageModel id: 1

		afterEach ->
			$('#fixtureContainer').empty()

		it "should have default properties", ->
			expect(@messageModel.get("greeting")).toEqual("Hello from Backbone")

		it "should have a url field that maps onto a server route", ->
			expect(@messageModel.url).toEqual("/api/message")

		it "should have different content in the greeting field after fetching from the server", ->
			@messageModel.fetch success: (model, response, options) =>
				@serviceReturn = true

			waitsFor(@waitForServiceReturn, 'service did not return', 1000)
			runs =>
				expect(@messageModel.get("greeting")).not.toEqual("Hello from Backbone")
				expect(@messageModel.get("greeting")).toEqual(greetingMessages.greetingMessage.greeting)


	describe "MessageList", ->
		beforeEach ->
			@waitForServiceReturn = ->
				typeof @serviceReturn != 'undefined'
			@messageList = new MessageList()

		afterEach ->
			$('#fixtureContainer').empty()

		it "should initially be empty", ->
			expect(@messageList.length).toEqual(0)

		it "should specify MessageModel as its model attribute", ->
			expect(@messageList.model).toEqual(MessageModel)

		it "should have a url field that maps onto a server route", ->
			expect(@messageList.url).toEqual("/api/messages")

		it "should contain an array of MessageModel objects after calling fetch()", ->
			@messageList.fetch success: (model, response, options) =>
				@serviceReturn = true

			waitsFor( @waitForServiceReturn, 'service did not return', 1000)
			runs: =>
				expect(@messageList.length).toBeGreaterThan(0)
				actualFirstMessage = @messageList.get(0)
				expectedFirstMessage = greetingMessages.greetingMessageList[0]
				expect(actualFirstMessage.get("greeting")).toEqual(expectedFirstMessage.greeting)

	describe "MessageController", ->
		describe "Default initialization", ->

			beforeEach ->
				@fixture = $('#fixtureContainer')
				@messageController = new MessageController
					el: @fixture
					model: new MessageModel()
				@messageController.render()

			afterEach ->
				$('#fixtureContainer').empty()

			it "should have template content that is rendered into its element", ->
				expect(@fixture.html()).not.toEqual("")
				expect(@fixture.html()).toContain("Greeting: Hello from Backbone")

	describe "MessageListController", ->
		describe "Default initialization", ->
			beforeEach ->
				@fixture = $('#fixtureContainer')
				messageList = new MessageList()

				@messageListController = new MessageListController
					el: @fixture
					collection: messageList
				messageList.set(greetingMessages.greetingMessageList)
				@messageListController.render()

			afterEach ->
				$('#fixtureContainer').empty()

			it "should have template content that is rendered into its element", ->
				expect(@fixture.html()).not.toEqual("")
				expect(@fixture.html()).toContain("Number of greetings")

			it "should render all the elements in it's collection", ->
				greetingList = @messageListController.$('.bv_listOfGreetings li')
				expect(greetingList.length).toEqual(2)
				expect($(greetingList[0]).html()).toContain("Good morning")

			it "should display the number of items in the list", ->

				expect($('.bv_itemCount').html()).toContain("2")