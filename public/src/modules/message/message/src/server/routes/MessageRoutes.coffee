exports.message = (req, res) ->
	if global.specRunnerTestmode
		greetingFixtures = require '../public/javascripts/spec/testFixtures/fixtures.js'
		res.writeHead(200, { 'Content-Type': 'application/json' })
		res.write JSON.stringify(greetingFixtures.greetingMessage)
		res.end()
	else
		res.writeHead(200, { 'Content-Type': 'application/json' })
		res.write JSON.stringify({greeting: "coming to you live from fake service"})
		res.end()

exports.messages = (req, res) ->
	console.log "global.specRunnerTestmode" + global.specRunnerTestmode
	if global.specRunnerTestmode
		greetingFixtures = require '../public/javascripts/spec/testFixtures/fixtures.js'
		res.writeHead(200, { 'Content-Type': 'application/json' })
		res.write JSON.stringify(greetingFixtures.greetingMessageList)
		res.end()
	else
		res.writeHead(200, { 'Content-Type': 'application/json' })
		res.write JSON.stringify([{greeting: "coming to you live from fake service"}, {greeting: "Message two"}])
		res.end()