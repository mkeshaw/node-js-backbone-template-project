class window.MessageModel extends Backbone.Model
	url: '/api/message'
	defaults:
		greeting: "Hello from Backbone"

	initialize: ->

class window.MessageList extends Backbone.Collection
	url: '/api/messages'
	model: MessageModel


class window.MessageController extends Backbone.View
	template: $('#message-cotroller-template')
	tagName: 'li'

	render: =>
		@$el.empty()
		template = _.template( $(@template).html(), @model.toJSON())
		@$el.html(template)

		# always return @ (reference to this objects 'this') to allow method chaining
		@

class window.MessageListController extends Backbone.View
	template: $('#message-list-controller-template').html()

	initialize: ->
		@collection.bind "fetch", @render
		@collection.bind "change", @render
		@collection.bind "add", @render

	render: =>
		@$el.empty()
		template = _.template( @template, {itemCount: @collection.length})
		@$el.html(template)
		@addAllItems()

		# always return @ (reference to this objects 'this') to allow method chaining
		@

	addAllItems: =>
		@collection.each (item) =>
			@addItem item

	addItem: (item) =>
		messageItem = new MessageController model: item
		@$('.bv_listOfGreetings').append(messageItem.render().el)
