(function() {
  var applicationScripts, requiredScripts;

  requiredScripts = ['src/lib/jquery.min.js', 'src/lib/json2.js', 'src/lib/underscore.js', 'src/lib/backbone-min.js'];

  applicationScripts = ['src/conf/configuration.js', 'javascripts/src/AppController.js', 'javascripts/src/Message.js'];

  exports.index = function(req, res) {
    "use strict";
    var scriptsToLoad;

    global.specRunnerTestmode = false;
    scriptsToLoad = requiredScripts.concat(applicationScripts);
    return res.render('index', {
      title: 'Message',
      scripts: scriptsToLoad
    });
  };

  exports.specRunner = function(req, res) {
    "use strict";
    var jasmineScripts, scriptsToLoad, specScripts;

    jasmineScripts = ['src/lib/testLibraries/jasmine-jstd-adapter/jasmine/lib/jasmine-core/jasmine.js', 'src/lib/testLibraries/jasmine-jstd-adapter/jasmine/lib/jasmine-core/jasmine-html.js', 'src/lib/testLibraries/jasmine-jquery/lib/jasmine-jquery.js', 'src/lib/testLibraries/sinon.js'];
    specScripts = ['javascripts/spec/AppControllerSpec.js', 'javascripts/spec/MessageSpec.js', 'javascripts/spec/testFixtures/MessageFixtures.js'];
    scriptsToLoad = requiredScripts.concat(jasmineScripts, specScripts);
    scriptsToLoad = scriptsToLoad.concat(applicationScripts);
    global.specRunnerTestmode = true;
    return res.render('SpecRunner', {
      title: 'Message SpecRunner',
      scripts: scriptsToLoad
    });
  };

}).call(this);
