(function() {
  exports.message = function(req, res) {
    var greetingFixtures;

    if (global.specRunnerTestmode) {
      greetingFixtures = require('../public/javascripts/spec/testFixtures/fixtures.js');
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.write(JSON.stringify(greetingFixtures.greetingMessage));
      return res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.write(JSON.stringify({
        greeting: "coming to you live from fake service"
      }));
      return res.end();
    }
  };

  exports.messages = function(req, res) {
    var greetingFixtures;

    console.log("global.specRunnerTestmode" + global.specRunnerTestmode);
    if (global.specRunnerTestmode) {
      greetingFixtures = require('../public/javascripts/spec/testFixtures/fixtures.js');
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.write(JSON.stringify(greetingFixtures.greetingMessageList));
      return res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.write(JSON.stringify([
        {
          greeting: "coming to you live from fake service"
        }, {
          greeting: "Message two"
        }
      ]));
      return res.end();
    }
  };

}).call(this);
